
<?php
include "config.php";
include "verifica.php";
?>
<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <link rel="stylesheet" type="text/css" href="css/estilo-gerenciaraluno.css" />
        <meta charset="UTF-8">
        <title>..:: ALUNOS ::..</title>
    </head>
    <body>
        <?php include "config.php"; ?>
        <div id="top">
            &nbsp;&nbsp;Gestão Escolar
        </div><!--div-top-->
        <div id="all">

            <div id="center">
                <div id="center-top">
                    <span id="sbv"><?php echo "Seja Bem-vindo <b>" . $_SESSION["nome_usuario"] . "</b>. "; ?></span>
                    <span id="confsair"> <a href="index.php"><img src="img/home.png" /></a> | <a href="sair.php"><img src="img/logout.png" /></a></span>
                </div><!--div-center-top-->

                <div id="center-content">
                    <form action="" name="rd" method="post">
                        Filtrar por: 
                        <input type="radio" name="nome" /> Nome
                        <input type="radio" name="fco" /> Código
                        <input type="radio" name="fma" /> Endereço
                        <input type="radio" name="fdt" /> Data/Nasc
                        <input type="submit" name="filtro" value="Filtrar" />
                        <?php
                        if (isset($_POST['filtro'])) {
                            if (isset($_POST['nome'])) {
                                $sqllista = mysql_query("SELECT * FROM aluno ORDER BY nome ASC");
                            } elseif (isset($_POST['fco'])) {
                                $sqllista = mysql_query("SELECT * FROM aluno ORDER BY id ASC");
                            } elseif (isset($_POST['fma'])) {
                                $sqllista = mysql_query("SELECT * FROM aluno ORDER BY endereco ASC");
                            } elseif (isset($_POST['fdt'])) {
                                $sqllista = mysql_query("SELECT * FROM aluno ORDER BY datanasc ASC");
                            }
                        } else {
                            // filtrado por padrão por descrição, caso nao seja escolhido o filtro.
                            $sqllista = mysql_query("SELECT * FROM aluno ORDER BY nome ASC");
                        }
                        ?>
                    </form>

                    <div id="lista">


                        <table border="0">
                                <!--<table border="0">-->
                            <thead style="position: relative;">
                                <tr class="titulo-tabela">
                                    <td width='34'>ID</td>
                                    <td width='322'>Nome</td>
                                    <td width='98'>Categoria</td>
                                    <td width='190'>Endereço</td>
                                    <td width='94'>Data/Nasc</td>
                                    <td width='94'>CPF</td>
                                    <td width='30'>Gerenciar</td>
                                </tr>
                            </thead>

                            <tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>
                            <tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>
                            <!--</table>-->
                            <?php
                            //$sqllista = mysql_query("SELECT * FROM produto ORDER BY descricao ASC");
                            //$sqlcategoria = mysql_query("SELECT * FROM categoria");
                            //$cat = mysql_fetch_array($sqlcategoria);


                            $cont = 0;

                            //$scat = mysql_query("SELECT nomecategoria FROM categoria");

                            while (($list = mysql_fetch_array($sqllista))) {
                                //$ctgr = "SELECT nomecategoria FROM categoria WHERE id = '" . $list['categoria'] . "'";

                                $idcat = $list['categoria'];

                                $qrycat = mysql_query("SELECT nomecategoria FROM categoria WHERE id = $idcat");
                                while ($categorize = mysql_fetch_array($qrycat)) {
                                    $listacategoria = $categorize['nomecategoria'];
                                }

                                if (($cont % 2) == 1) {
                                    $cor = '#ccc;';
                                } else {
                                    $cor = '#ffffff;';
                                }
                                echo "<tr style='background-color:" . $cor . "'>";
                                echo "<td width='18'> " . $list['id'] . " </td>";
                                echo "<td width='160'> " . $list['nome'] . " </td>";
                                echo "<td width='52'> " . $listacategoria . " </td>";
                                echo "<td width='100'> " . $list['endereco'] . " </td>";
                                echo "<td width='50'> " . implode('/', array_reverse(explode('-', $list["datanasc"]))) . " </td>";
                                echo "<td width='100'> " . $list['cpf'] . " </td>";
                                echo "<td width='40' class='mng'> <a href=\"altera.php?id=" . $list['id'] . "\"><img src='img/updt-mini.png' title='Alterar' /></a> &nbsp; <a href=\"deleta.php?id=" . $list['id'] . "\"><img src='img/delete-mini.png' title='Deletar' /></a> </td>";
                                echo "</tr>";
                                $cont = $cont + 1;
                            }
                            ?>



                        </table>


                    </div>
                    <div id="formulario">

                        <!--dando início ao formulário, com o metodo POST, o código está sendo efetuado
                        atravéz de um isset na verificação do hidden gravar -->
                        <script type="text/javascript">
                            function mascaraCpf(o, f) {
                                v_obj = o
                                v_fun = f
                                setTimeout('execmascaraa()', 2)
                            }

                            function execmascaraa() {
                                v_obj.value = v_fun(v_obj.value)
                            }
                            function cpfUser(v) {
                                v = v.replace(/\D/g, "")

                                if (v.length > 11) {
                                    return "apenas 11 dígitos"
                                } else {

                                    v = v.replace(/(\d{3})(\d)/, "$1.$2")
                                    v = v.replace(/(\d{3})(\d)/, "$1.$2")
                                    v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2")
                                }
                                return v

                            }
                        </script>

                        <form action="" method="POST">
                            <input type="hidden" name="gravar" value="gravar" /> 
                            <fieldset class="fieldset">
                                <legend>Cadastro de Aluno</legend>
                                <table border="0" id="tb01">
                                    <tr>
                                        <td class="c1">Nome: </td><td class="c2"><input type="text" name="desc" id="desc" placeholder="Entre com o nome do aluno"/></td>
                                    </tr>
                                    <tr>
                                        <td class="c1">Categoria: </td>
                                        <td class="c2">
                                            <select name="lista1" id="lista1">
<?php
//os valores do select estão sendo listados
//da tabela categoria no banco de dados.
$consulta = mysql_query("SELECT * FROM categoria order by nomecategoria ASC");
while ($dados = mysql_fetch_array($consulta)) {
    //o value resgata o id, que fica salvo no bd pois é chave estrangeira.
    //entre as tags option ele resgata o nomecategoria.
    echo "<option value='" . $dados['id'] . "'>" . $dados['nomecategoria'] . "</option>";
}
?>
                                            </select>


                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="c1">Endereço: </td><td class="c2"><input type="text" name="endereco" id="marca" placeholder="Entre com o endereço" /></td>
                                    </tr>
                                    <tr>
                                        <td class="c1">CPF: </td><td class="c2"><input onkeypress="mascaraCpf(this, cpfUser)" onBlur="clearTimeout()" name="cpf" id="cpf" type="text" /></td>
                                    </tr>
                                    <tr>
                                        <td class="c1">Data de Nascimento: </td><td class="c2"><input type="date" name="datanasc" id="datafab" placeholder="yyyy-mm-dd" /></td>
                                    </tr>
                                    <tr>
                                            <td class="c1"></td><td class="c2"><input type="submit" name="cadprod" value="Efetuar Cadastro" /> <!--<input type="submit" name="voltar" value="voltar" /></td>-->
                                    </tr>
<?php
if (isset($_POST['voltar'])) {
    header("Location:index.php");
}
?>




                                </table>
                            </fieldset>
                        </form>
<?php
// inclusão do arquivo de conexão.
include "config.php";
// atribuição dos valores do formulário as variáveis.
// uso do @ para omitir o erro de undefined index.
$nome = @$_POST['desc'];
$categoria = @$_POST['lista1'];
$endereco = @$_POST['endereco'];
$dataf = @$_POST['datanasc'];
$cpf = @$_POST['cpf'];
$datadehj = date('Y-m-d');



if (isset($_POST['gravar'])) {
    // inicio da validação.
    if (empty($nome)) {
        echo "Informe o nome do aluno";
    } elseif (empty($endereco)) {
        echo "Informe o endereço";
    } elseif (empty($dataf)) {
        echo "Informe a data";
    } elseif (empty($cpf)) {
        echo "Informe o CPF";
    } elseif ($cpf == "apenas 11 dígitos") {
        echo "Digite um CPF válido";
    } elseif (!$cpf = str_pad(ereg_replace('[^0-9]', '', $cpf), 11, '0', STR_PAD_LEFT)) {
        echo "CPF Inválido";
    } elseif (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
        echo "CPF Inválido";
    } else {

        $vcpf = "SELECT * FROM aluno WHERE cpf = '$cpf'";
        $rcpf = mysql_query($vcpf);
        $num = mysql_num_rows($rcpf);
        if ($num > 0) {
            echo "<script> alert('CPF Ja Existente')</script>";
        }else{
            //fim da validação.

            $sqlcad = "INSERT INTO aluno (nome, categoria, endereco, datanasc, cpf, data_cadastro) VALUES ('$nome', '$categoria', '$endereco', '$dataf', '$cpf', '$datadehj')";
            mysql_query($sqlcad);

            if ($sqlcad) {
                echo 'Cadastro efetuado com sucesso.';
                echo "<meta HTTP-EQUIV='refresh' CONTENT='0'>";
            } else {
                echo 'Problemas ao tentar efetuar cadastro.';
            }
        
        }
            }
    }
    ?>

                    </div><!--div-formulario-->


                    <form method='post' action='index.php'>		
                        <input type='submit' name='vol' value='voltar' />					
                    </form>




                </div><!--div-center-content-->


            </div><!--div-center-->
        </div><!--div-all-->

    </body>
</html>