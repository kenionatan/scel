<?php

	include "config.php";
	include "verifica.php";
?>
<!DOCTYPE HTML>
<html lang="pt-br">
<head>
	<link rel="stylesheet" type="text/css" href="css/estilo-index.css" />
	<meta charset="UTF-8">
	<title>..:: Início ::..</title>
</head>
<body>
	<div id="top">
			&nbsp;&nbsp;Gestão Escolar
		</div><!--div-top-->
	<div id="all">
		
		<div id="center">
			<div id="center-top">
			<span id="sbv"><?php echo "Seja Bem-vindo <b>".$_SESSION["nome_usuario"]."</b>. "; ?></span>
			<span id="confsair"><a href="empresa/index.php">Empresa</a> | <a href="index.php"><img src="img/home.png" /></a> | <a href="sair.php"><img src="img/logout.png" /></a></span>
			</div><!--div-center-top-->

			<div id="center-content">
				
				<div id="logo">
					<center>
					<br />
					<br />
					<img src="img/scel.jpeg" /><br />
					<font size=4><b>SCEL - IRES</b></font>
					<h1 class="logo-titulo">Sistema de Controle para Escola de Libras</h1><p>1.0</p>
					<br />
					<br />
					<br />
					<img src="img/aaaa.png" />
					</center>
				</div><!--div-logo-->
				<br />
				<div id="line1">
				</div><!--div-line1-->

				<div id="menu">
					<center>
						<table border="0" cellpadding="4" cellspacing="8" class="tabela">
							<tr>
								<td class="titulo-menu">
								Cadastro	
								</td>
							</tr>	
							<tr>
								
								<td>
								<a href="gerenciar-aluno.php">
								<img src="img/aluno.png" />
								<br />
								Aluno
								</a>
								</td>
								

								<td>
								<a href="gerenciar-professor.php">
								<img src="img/pro.png" />
								<br />
								Professor
								</a>
								</td>

								<td>
								<a href="cursos.php">
								<img src="img/cursos.png" />
								<br />
								Cursos
								</a>
								</td>
							
							</tr>
							<tr>
								<td class="titulo-menu">
								Controle
								</td>
							</tr>
							<tr>
                            	<td>
								<a href="turma.php">
								<img src="img/enroll.png" />
								<br />
								&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;
                                Turma
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;
								</a>	
								</td>
								<td>
								<a href="matricular-aluno.php">
								<img src="img/ma.png" />
								<br />
								Matricular Aluno
								</a>	
								</td>
								<td>
								<a href="acompanhar-turma.php">
								<img src="img/check.png" />
								<br />
								Acompanhar Turma
								</a>
								</td>
								<!--<td>
								<img src="img/list-user.png" />
								<br />
								Listar Usuários	
								</td>-->
							</tr>
							
							<tr>
								<td class="titulo-menu">
								Gerenciador
								</td>
							</tr>
							<tr>
								<td>
								<a href="lista-relatorios.php">
								<img src="img/list-blue.png" />
								<br />
								Relatórios
								</a>
								</td>

								<td>
								<a href="cadastrar-usuario.php">
								<img src="img/userid.png" />
								<br />
								Usuários
								</a>
								</td>
								
								<td>
								<a href="professor.php">
								<img src="img/parameters.png" />
								<br />
								Parâmetros
								</a>
								</td>
							
							</tr>
							<!--<tr>
								<td class="titulo-menu">
								Movimento
								</td>
							</tr>
							<tr>
								<td>
								<img src="img/product-in.png" />
								<br />
								Entrada
								</td>
								<td>
								<img src="img/product-out.png" />
								<br />
								Saída
								</td>
							</tr>-->	
						</table>
					</center>
				</div><!--div-menu-->

			</div>

		</div><!--div-center-->
	</div><!--div-all-->

</body>
</html>