<?php

	include "config.php";
	include "verifica.php";
?>
<!DOCTYPE HTML>
<html lang="pt-br">
<head>
	<link rel="stylesheet" type="text/css" href="css/estilo-gerenciarprofessor.css" />
	<meta charset="UTF-8">
	<title>..:: PROFESSORES ::..</title>
</head>
<body>
	<?php include "config.php"; ?>
	<div id="top">
			&nbsp;&nbsp;Gestão Escolar
		</div><!--div-top-->
	<div id="all">
		
		<div id="center">
			<div id="center-top">
			<span id="sbv"><?php echo "Seja Bem-vindo <b>".$_SESSION["nome_usuario"]."</b>. "; ?></span>
			<span id="confsair"> <a href="index.php"><img src="img/home.png" /></a> | <a href="sair.php"><img src="img/logout.png" /></a></span>
			</div><!--div-center-top-->

			<div id="center-content">
				 <form action="" name="rd" method="post">
                        Filtrar por: 
                        <input type="radio" name="nome" /> Nome
                        <input type="radio" name="id" /> Código
                        <input type="radio" name="end" /> Endereço
                        <input type="submit" name="filtro" value="Filtrar" />
                        <?php
                        if (isset($_POST['filtro'])) {
                            if (isset($_POST['nome'])) {
                                $sqllista = mysql_query("SELECT * FROM professor ORDER BY nome ASC");
                            } elseif (isset($_POST['id'])) {
                                $sqllista = mysql_query("SELECT * FROM professor ORDER BY id ASC");
                            } elseif (isset($_POST['end'])) {
                                $sqllista = mysql_query("SELECT * FROM professor ORDER BY endereco ASC");
                        } 
                    }
                        else {
                            // filtrado por padrão por descrição, caso nao seja escolhido o filtro.
                            $sqllista = mysql_query("SELECT * FROM professor ORDER BY nome ASC");
                        }
                    
                        ?>
                    </form>
				
				<div id="lista">
					
					
					<table border="0">
						<!--<table border="0">-->
					<thead style="position: relative;">
						<tr class="titulo-tabela">
							<td width='24'>ID</td>
							<td width='322'>Nome</td>
							<td width='290'>Endereço</td>
							<td width='90'>CPF</td>
							<td width='20'>Gerenciar</td>
						</tr>
					</thead>

					<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>
					<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>
					<!--</table>-->
					<?php
						$sqllista = mysql_query("SELECT * FROM professor ORDER BY nome ASC");

						//$sqlcategoria = mysql_query("SELECT * FROM categoria");
						//$cat = mysql_fetch_array($sqlcategoria);
						

						$cont = 0;

						//$scat = mysql_query("SELECT nomecategoria FROM categoria");

						while (($list = mysql_fetch_array($sqllista))) {
							//$ctgr = "SELECT nomecategoria FROM categoria WHERE id = '" . $list['categoria'] . "'";

								if(($cont % 2) == 1){
									$cor = '#ccc;';
								}else{
									$cor = '#ffffff;';
								}
								echo "<tr style='background-color:".$cor."'>";
								echo "<td width='18'> ".$list['id']." </td>";
								echo "<td width='160'> ".$list['nome']." </td>";
								echo "<td width='100'> ".$list['endereco']." </td>";
								echo "<td width='90'> ".$list['cpf']." </td>";
								echo "<td width='40' class='mng'> <a href=\"altera-professor.php?id=".$list['id']."\"><img src='img/updt-mini.png' title='Alterar' /></a> &nbsp; <a href=\"deleta-professor.php?id=".$list['id']."\"><img src='img/delete-mini.png' title='Deletar' /></a> </td>";
								echo "</tr>";
								$cont = $cont + 1;
								
						}							
						
						?>
						
					
					
					</table>


				</div>
				<div id="formulario">

					<!--dando início ao formulário, com o metodo POST, o código está sendo efetuado
					atravéz de um isset na verificação do hidden gravar -->
				
				<form action="" method="POST">
					<input type="hidden" name="gravar" value="gravar" /> 
					<fieldset class="fieldset">
					<legend>Cadastro de Professor</legend>
					<table border="0" id="tb01">
						<tr>
							<td class="c1">Nome: </td><td class="c2"><input type="text" name="nomep" id="nomep" placeholder="Entre com o nome do professor"/></td>
						</tr>

						<tr>
							<td class="c1">Endereço: </td><td class="c2"><input type="text" name="enderecop" id="enderecop" placeholder="Entre com o endereço" /></td>
						</tr>
                        <tr>
							<td class="c1">CPF: </td><td class="c2"><input type="text" name="cpfp" id="cpfp" placeholder="000.000.000-00" /></td>
						</tr>
						<tr>
							<td class="c1"></td><td class="c2"><input type="submit" name="cadprod" value="Efetuar Cadastro" /> <!--<input type="submit" name="voltar" value="voltar" /></td>-->
						</tr>
						<?php
							if (isset($_POST['voltar'])) {
								header("Location:index.php");
							}
						?>

						
						

					</table>
					</fieldset>
				</form>
				<?php
						// inclusão do arquivo de conexão.
						include "config.php";
						// atribuição dos valores do formulário as variáveis.
						// uso do @ para omitir o erro de undefined index.
						$nomepr = @$_POST['nomep'];
						$enderecopr = @$_POST['enderecop'];
						$cpfpr = @$_POST['cpfp'];
                                                $datadehoje = date('Y-m-d');

						

						if(isset($_POST['gravar'])){
							// inicio da validação.
							if(empty($nomepr)){
								echo "Informe o nome do professor";
							}elseif (empty($enderecopr)) {
								echo "Informe o endereço";
							}else{
								//fim da validação.

							$sqlcad = "INSERT INTO professor (nome, endereco, cpf, data_cadastro) VALUES ('$nomepr', '$enderecopr','$cpfpr', '$datadehoje')";
							mysql_query($sqlcad);

							if($sqlcad){
								echo 'Cadastro efetuado com sucesso.';
								echo "<meta HTTP-EQUIV='refresh' CONTENT='0'>";
							}else{
								echo 'Problemas ao tentar efetuar cadastro.';
							}
							}
						}

				?>
				
				</div><!--div-formulario-->


				<form method='post' action='index.php'>		
						<input type='submit' name='vol' value='voltar' />					
				</form>




			</div><!--div-center-content-->
				

		</div><!--div-center-->
	</div><!--div-all-->
</body>
</html>