<!DOCTYPE HTML>
<html lang="pt-br">
<head>
	<link rel="stylesheet" type="text/css" href="css/estilo-login.css" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
	<meta charset="UTF-8">
	<title>..:: Login ::..</title>
</head>
<body>
	<div id="top">
			&nbsp;&nbsp;Gestão Escolar
	</div><!--div-top-->
	<div id="all">
		
		<div id="center">
			<div id="center-top">
			<span id="sbv"> Seja Bem-vindo. </span>
			<span id="confsair"><a href="ajuda.txt">Ajuda</a></span>
			</div><!--div-center-top-->

			<div id="center-content">

				<div id="form-login">
					<form action="" method="POST">
						<table border="0">
							<legend>Bem-Vindo, Efetue o Login</legend>
							<tr>
								<td><input type="text" name="login" id="login" placeholder="Login" /></td>
							</tr>
							<tr>
								<td><input type="password" name="senha" id="senha" placeholder="Senha" /></td>
							</tr>
							<tr>
								<td><input type="submit" name="logar" id="logar" value="Logar" /></td>
							</tr>
						</table>
					</form>

					<?php
					include "config.php";

					if (isset($_POST['logar'])) {
						

						session_start("escola");

						$log = isset($_POST["login"]) ? addslashes(trim($_POST["login"])) : FALSE;
						$sen = isset($_POST["senha"]) ? addslashes(trim($_POST["senha"])) : FALSE;

						if(!$log || !$sen){
							echo "*Você deve digitar seu login e senha.";
							exit;
						}

						$sqllogin = "SELECT id, nomedousuario, login, senha FROM login WHERE login = '" . $log . "'";
						$result_id = @mysql_query($sqllogin) or die("Erro no Banco de dados!");
						$total = @mysql_num_rows($result_id);


						if ($total) {
							$dadosuser = @mysql_fetch_array($result_id);
							//echo $dadosuser[3];

							if (!strcmp($sen, $dadosuser[3])){
								$_SESSION["id_usuario"] = $dadosuser["id"];
								$_SESSION["login_usuario"] = $dadosuser["login"];
								$_SESSION["nome_usuario"] = stripslashes($dadosuser["nomedousuario"]);

								header("Location: index.php");
								exit;
							}else{
								echo "* Senha Inválida.";
								exit;
							}
						}else{
							echo "* O Login fornecido não existe.";
							exit;
						}

						
					}
					?>

				</div><!--div-form-login-->	
				
				
			</div><!--div-center-content-->

		</div><!--div-center-->
	</div><!--div-all-->

</body>
</html>