-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 04/06/2013 às 22:02:30
-- Versão do Servidor: 5.5.27
-- Versão do PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `sistemavendas`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nomecategoria` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`id`, `nomecategoria`) VALUES
(1, 'Equipamento para Notebook'),
(2, 'Equipamento de Desktop'),
(3, 'Bags & Mochilas'),
(4, 'Dispositivos de E/S'),
(5, 'Dispositivos de MultimÃ­dia');

-- --------------------------------------------------------

--
-- Estrutura da tabela `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nomedousuario` varchar(40) NOT NULL,
  `login` varchar(20) NOT NULL,
  `senha` varchar(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `login`
--

INSERT INTO `login` (`id`, `nomedousuario`, `login`, `senha`) VALUES
(1, 'Kenio Natan', 'kenio', '4321'),
(2, 'Kleverson Leonardo', 'kleverson', '4321'),
(3, 'João Silva', 'joao', '1234'),
(4, 'Wescly Ferreira', 'wescly', '1111'),
(5, 'Pedro Silva', 'pedro', 'pedro'),
(6, 'Visitante', 'teste', 'teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE IF NOT EXISTS `produto` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) NOT NULL,
  `categoria` int(11) NOT NULL,
  `preco` double NOT NULL,
  `marca` varchar(30) NOT NULL,
  `datafab` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categoria` (`categoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `descricao`, `categoria`, `preco`, `marca`, `datafab`) VALUES
(4, 'mouse', 1, 16, 'clone', '2013-05-12'),
(5, 'teclado p2p', 2, 22, 'leadership', '2013-05-01'),
(9, 'HD SATA 160GB', 1, 216.49, 'samsung', '2012-03-02'),
(11, 'Placa MÃ£e', 2, 120, 'Intel', '2013-05-12'),
(12, 'MemÃ³ria RAM DDR2 4GB', 2, 120, 'kingston', '2012-02-28'),
(13, 'HD SATA 500GB', 1, 316.99, 'samsung', '2012-02-28'),
(14, 'Pen Drive 4GB', 4, 16, 'leadership', '2013-05-12'),
(15, 'Pen Drive 2GB', 4, 10, 'kingston', '2013-05-01'),
(16, 'Pen Drive 8GB', 4, 22, 'kingston', '2013-05-12'),
(17, 'Pen Drive 16GB', 4, 30, 'kingston', '2013-05-01'),
(18, 'CartÃ£o de MemÃ³ria 2GB', 4, 30, 'kingston', '2013-05-12'),
(19, 'CartÃ£o de MemÃ³ria 4GB', 4, 45, 'kingston', '2013-05-12'),
(20, 'CartÃ£o de MemÃ³ria 1GB', 4, 20, 'kingston', '2013-05-12'),
(21, 'Adaptador para CartÃ£o de MemÃ³ria mini SD', 4, 10, 'LG', '2013-05-12'),
(22, 'Adaptador para CartÃ£o de MemÃ³ria mini SD', 4, 14, 'kingston', '2013-05-12'),
(23, 'Adaptador USB para CartÃ£o mini SD', 4, 7, 'LG', '2013-05-12'),
(24, 'Adaptador USB para CartÃ£o SD', 4, 8, 'LG', '2013-05-12'),
(25, 'Headfones slim', 5, 12, 'leadership', '2012-02-28'),
(26, 'Headset p2', 5, 14, 'clone', '2012-02-28'),
(27, 'Adaptador 3 saÃ­das p2 1 entrada p10', 5, 16, 'clone', '2012-03-02'),
(28, 'Console Headset', 5, 65, 'intelbras', '2013-05-01');

--
-- Restrições para as tabelas dumpadas
--

--
-- Restrições para a tabela `produto`
--
ALTER TABLE `produto`
  ADD CONSTRAINT `produto_ibfk_1` FOREIGN KEY (`categoria`) REFERENCES `categoria` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
